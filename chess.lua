_G.log = {}
-- libraries
--------------------------------------------------------------------------------

local component = require("component")
local event = require("event")

-- configuration
--------------------------------------------------------------------------------

local projectors = require("projectors")
local screens = require("screens")
local DEBUG = true

local palette = {
  blackBG = 0x211915,
  blackBGSelected = 0x1D3420,
  blackBGPrevMove = 0x3E2836,
  blackFG = 0x314A4E,

  whiteBG = 0x314A4E,
  whiteBGSelected = 0x38633E,
  whiteBGPrevMove = 0x644055,
  whiteFG = 0x211915,
}

-- components
--------------------------------------------------------------------------------

local secondaryScreen = component.screen.address
local gpu = component.gpu

-- chess constants
--------------------------------------------------------------------------------

local WHITE = 2
local BLACK = 1

local PAWN = "p"
local KNIGHT = "n"
local BISHOP = "b"
local ROOK = "r"
local QUEEN = "q"
local KING = "k"

local CASTLING_KINGSIDE = 1
local CASTLING_QUEENSIDE = 2

-- utils
--------------------------------------------------------------------------------

local function sign(num)
  if num > 0 then
    return 1
  elseif num < 0 then
    return -1
  else
    return 0
  end
end

-- 2d array utils
--------------------------------------------------------------------------------

local function idx2dx64(x, y)
  return (y - 1) * 8 + x
end

local function idx2dx16(x, y)
  return (y - 1) * 4 + x
end

-- board handling
--------------------------------------------------------------------------------

local function getCellColor(x, y)
  return (x + y) % 2 + 1
end

local function getOppositeColor(color)
  return 3 - color
end

local function setPiece(board, x, y, kind, color)
  board[idx2dx64(x, y)] = { kind, color }
end

local function getPiece(board, x, y)
  return table.unpack(board[idx2dx64(x, y)] or {})
end

local function getPieceKind(board, x, y)
  local piece = board[idx2dx64(x, y)]
  return piece and piece[1]
end

local function getPieceColor(board, x, y)
  local piece = board[idx2dx64(x, y)]
  return piece and piece[2]
end

local function movePiece(board, x0, y0, x1, y1)
  local idx0 = idx2dx64(x0, y0)
  local piece = board[idx0]
  board[idx0] = nil
  board[idx2dx64(x1, y1)] = piece
end

local function setStartingPieces(board)
  setPiece(board, 1, 1, ROOK, WHITE)
  setPiece(board, 8, 1, ROOK, WHITE)
  setPiece(board, 1, 8, ROOK, BLACK)
  setPiece(board, 8, 8, ROOK, BLACK)
  setPiece(board, 2, 1, KNIGHT, WHITE)
  setPiece(board, 7, 1, KNIGHT, WHITE)
  setPiece(board, 2, 8, KNIGHT, BLACK)
  setPiece(board, 7, 8, KNIGHT, BLACK)
  setPiece(board, 3, 1, BISHOP, WHITE)
  setPiece(board, 6, 1, BISHOP, WHITE)
  setPiece(board, 3, 8, BISHOP, BLACK)
  setPiece(board, 6, 8, BISHOP, BLACK)
  setPiece(board, 4, 1, QUEEN, WHITE)
  setPiece(board, 4, 8, QUEEN, BLACK)
  setPiece(board, 5, 1, KING, WHITE)
  setPiece(board, 5, 8, KING, BLACK)

  for x = 1, 8 do
    setPiece(board, x, 2, PAWN, WHITE)
    setPiece(board, x, 7, PAWN, BLACK)
  end
end

local function newBoard()
  local board = {}

  setStartingPieces(board)

  board.castlingRights = {
    [WHITE] = {
      [CASTLING_KINGSIDE] = true,
      [CASTLING_QUEENSIDE] = true,
    },
    [BLACK] = {
      [CASTLING_KINGSIDE] = true,
      [CASTLING_QUEENSIDE] = true,
    }
  }

  board.enPassantTarget = nil

  return board
end

-- move generation
--------------------------------------------------------------------------------

local potentialMoveGenerators = {
  [ROOK] = function(board, x0, y0, callback)
    for i = 1, 8, 1 do
      callback(i, y0)
      callback(x0, i)
    end
  end,

  [KNIGHT] = function(board, x0, y0, callback)
    local offsets = {
                {-1,  2}, {1,  2},
      {-2,  1},                    {2,  1},
      {-2, -1},                    {2, -1},
                {-1, -2}, {1, -2},
    }

    for _, offset in ipairs(offsets) do
      callback(x0 + offset[1], y0 + offset[2])
    end
  end,

  [QUEEN] = function(board, x0, y0, callback)
    for i = 1, 7, 1 do
      callback(x0 + i, y0)
      callback(x0 - i, y0)
      callback(x0, y0 + i)
      callback(x0, y0 - i)
      callback(x0 + i, y0 + i)
      callback(x0 + i, y0 - i)
      callback(x0 - i, y0 + i)
      callback(x0 - i, y0 - i)
    end
  end,

  [KING] = function(board, x0, y0, callback)
    for x = x0 - 1, x0 + 1, 1 do
      for y = y0 - 1, y0 + 1, 1 do
        if x ~= x0 or y ~= y0 then
          callback(x, y)
        end
      end
    end

    -- castling
    callback(x0 - 2, y0)
    callback(x0 + 2, y0)
  end,

  [BISHOP] = function(board, x0, y0, callback)
    for i = 1, 7, 1 do
      callback(x0 + i, y0 + i)
      callback(x0 + i, y0 - i)
      callback(x0 - i, y0 + i)
      callback(x0 - i, y0 - i)
    end
  end,

  [PAWN] = function(board, x0, y0, callback)
    -- pawns are the worst pieces: their moves depend on their color
    if getPieceColor(board, x0, y0) == WHITE then
      callback(x0, y0 + 1)
      callback(x0, y0 + 2)
      callback(x0 - 1, y0 + 1)
      callback(x0 + 1, y0 + 1)
    else
      callback(x0, y0 - 1)
      callback(x0, y0 - 2)
      callback(x0 - 1, y0 - 1)
      callback(x0 + 1, y0 - 1)
    end
  end,
}

local function isCellObstructed(board, x, y, currentColor, canCapture)
  return getPieceKind(board, x, y) ~= nil and
    (not canCapture or getPieceColor(board, x, y) == currentColor)
end

local function isPathObstructed(board, x0, y0, x1, y1, currentColor, noCapture)
  local x = x0
  local y = y0
  local xs = sign(x1 - x0)
  local ys = sign(y1 - y0)
  repeat
    if (x ~= x0 or y ~= y0) and isCellObstructed(
        board, x, y, currentColor,
        not noCapture and x == x1 and y == y1
      )
    then
      return true
    end
    x = x + xs
    y = y + ys
  until x == x1 + xs and y == y1 + ys

  return false
end

local MOVE_ILLEGAL = 1
local MOVE_REGULAR = 2
local MOVE_PAWN_DOUBLE_ADVANCE = 3
local MOVE_CAPTURE = 4
local MOVE_EN_PASSANT_CAPTURE = 5
local MOVE_CASTLING_QUEENSIDE = 6
local MOVE_CASTLING_KINGSIDE = 7

local function isCellAttacked(board, x, y, color)
  -- error("TODO: unimplemented")
  return false
end

local function isCastlingValid(board, color, kind)
  if not board.castlingRights[color][kind] then
    return false
  end

  local isKingside = kind == CASTLING_KINGSIDE

  local y = color == WHITE and 1 or 8
  local kx0 = 5
  local kx1 = isKingside and 7 or 3
  local rx0 = isKingside and 8 or 1
  local rx1 = isKingside and 6 or 4

  if isPathObstructed(board, rx0, y, rx1, y, color, true) then
    return false
  end

  for x = kx0, kx1, sign(kx1 - kx0) do
    if isCellAttacked(board, x, y, color) then
      return false
    end
  end

  return true
end

local moveValidators = {
  [ROOK] = function(board, x0, y0, x1, y1, color)
    return not isPathObstructed(board, x0, y0, x1, y1, color)
  end,

  [KNIGHT] = function(board, x0, y0, x1, y1, color)
    return not isCellObstructed(board, x1, y1, color, true)
  end,

  [QUEEN] = function(board, x0, y0, x1, y1, color)
    return not isPathObstructed(board, x0, y0, x1, y1, color)
  end,

  [KING] = function(board, x0, y0, x1, y1, color)
    if isPathObstructed(board, x0, y0, x1, y1, color) then
      return false
    end

    if math.abs(x1 - x0) == 2 then
      local castlingKind = x1 > x0 and CASTLING_KINGSIDE or CASTLING_QUEENSIDE

      if isCastlingValid(board, color, castlingKind) then
        if castlingKind == CASTLING_QUEENSIDE then
          return MOVE_CASTLING_QUEENSIDE
        else
          return MOVE_CASTLING_KINGSIDE
        end
      end

      return false
    end

    -- checks are accounted for in the outer function
    return true
  end,

  [BISHOP] = function(board, x0, y0, x1, y1, color)
    return not isPathObstructed(board, x0, y0, x1, y1, color)
  end,

  [PAWN] = function(board, x0, y0, x1, y1, color)
    if math.abs(y1 - y0) == 2 and y0 ~= 2 and y0 ~= 7 then
      return false
    end

    if x0 == x1 then
      if isPathObstructed(board, x0, y0, x1, y1, color, true) then
        return false
      end

      if math.abs(y1 - y0) == 2 then
        return MOVE_PAWN_DOUBLE_ADVANCE
      end

      return true
    end

    local capturedColor = getPieceColor(board, x1, y1)

    if not capturedColor then
      -- don't lose hope! stay determined... and look for en passant
      if board.enPassantTarget == idx2dx64(x1, y1) then
        return true
      end

      return false
    end

    return true
  end,
}

-- Checks that the move is valid.
local function classifyMove(board, x0, y0, x, y)
  local piece, color = getPiece(board, x0, y0)

  if x > 8 or y > 8 or x < 1 or y < 1 then
    return MOVE_ILLEGAL
  end

  if not piece then
    -- unfortunately, moving an empty square doesn't achieve anything
    return MOVE_ILLEGAL
  end

  if x0 == x and y0 == y then
    -- as fun as it would be, you can't not move a piece
    return MOVE_ILLEGAL
  end

  local isMoveEvenPossible = false

  potentialMoveGenerators[piece](board, x0, y0, function(px, py)
    if px == x and py == y then
      isMoveEvenPossible = true
    end
  end)

  if not isMoveEvenPossible then
    -- someone please teach the caller how to play chess
    return MOVE_ILLEGAL
  end

  local result = moveValidators[piece](board, x0, y0, x, y, color)

  if not result then
    return MOVE_ILLEGAL
  end

  -- -- local conjecturedPosition = board:clone()
  -- -- conjecturedPosition:moveUnchecked(x0, y0, x, y)
  -- -- local blackKing, whiteKing = conjecturedPosition:findChecks()

  -- -- if (color == BLACK and blackKing) or (color == WHITE and whiteKing) then
  -- --   -- the move puts the king into check
  -- --   return MOVE_ILLEGAL
  -- -- end

  if board.enPassantTarget == idx2dx64(x, y) then
    return MOVE_EN_PASSANT_CAPTURE
  end

  local captureColor = getPieceColor(board, x, y)
  if captureColor then
    if captureColor == getOppositeColor(color) then
      return MOVE_CAPTURE
    else
      return MOVE_ILLEGAL
    end
  end

  if result == true then
    result = MOVE_REGULAR
  end

  return result
end

local function getValidMoves(board, x0, y0)
  local piece = getPieceKind(board, x0, y0)
  if not piece then
    return {}
  end

  local moves = {}

  potentialMoveGenerators[piece](board, x0, y0, function(x, y)
    if classifyMove(board, x0, y0, x, y) ~= MOVE_ILLEGAL then
      table.insert(moves, {x, y})
    end
  end)

  return moves
end

-- palette handling
--------------------------------------------------------------------------------

local function savePalette()
  local palette = {}
  for i = 0, 15 do
    palette[i] = gpu.getPaletteColor(i)
  end
  return palette
end

local function restorePalette(palette)
  for i = 0, 15 do
    gpu.setPaletteColor(i, palette[i])
  end
end

local function setupPalette()
  local idx = 0
  local cache = {}
  for _, color in pairs(palette) do
    if not cache[color] then
      cache[color] = true
      gpu.setPaletteColor(idx, color)
      idx = idx + 1
    end
  end
end

-- gpu state
--------------------------------------------------------------------------------

local function saveGpuState()
  local state = {}
  state.screen = gpu.getScreen()
  state.resolution = {gpu.getResolution()}
  state.palette = savePalette()
  state.background = gpu.getBackground()
  state.foreground = gpu.getForeground()
  return state
end

local function restoreGpuState(state)
  gpu.bind(state.screen, false)
  restorePalette(state.palette)
  gpu.setResolution(table.unpack(state.resolution))
  gpu.setBackground(state.background)
  gpu.setForeground(state.foreground)
end

local cachedBackground = -1
local function setBackground(background)
  if background ~= cachedBackground then
    cachedBackground = background
    gpu.setBackground(background)
  end
end

local cachedForeground = -1
local function setForeground(foreground)
  if foreground ~= cachedForeground then
    cachedForeground = foreground
    gpu.setForeground(foreground)
  end
end

local function gpuBind(screen, reset)
  cachedBackground = -1
  cachedForeground = -1
  gpu.bind(screen, reset)
end

-- drawing board cells
--------------------------------------------------------------------------------

local function getCellScrPos(x, y)
  return x * 10 - 9, 16 - ((y - 1) % 4) * 5
end

local function drawCell(x, y, bg, fg)
  setBackground(bg)
  if x == 1 or y == 1 then
    setForeground(fg)
  end

  local scrX, scrY = getCellScrPos(x, y)

  gpu.fill(scrX, scrY, 10, 5, " ")

  if x == 1 then
    gpu.set(1, scrY, tostring(y))
  end

  if y == 1 then
    gpu.set(scrX + 9, 20, string.char(96 + x))
  end
end

local function drawMarker(x, y, bg)
  setBackground(bg)
  local scrX, scrY = getCellScrPos(x, y)
  gpu.fill(scrX + 4, scrY + 2, 2, 1, " ")
end

local function initBoardScreen(idx)
  gpuBind(screens[idx])
  gpu.setResolution(80, 20)
  setupPalette()
end

local cachedBoardCells = {}

local HIGHLIGHT_NORMAL = 1
local HIGHLIGHT_SELECTED = 2
local HIGHLIGHT_PREV_MOVE = 3

local CELL_BACKGROUNDS = {
  [WHITE] = {
    [HIGHLIGHT_NORMAL] = palette.whiteBG,
    [HIGHLIGHT_SELECTED] = palette.whiteBGSelected,
    [HIGHLIGHT_PREV_MOVE] = palette.whiteBGPrevMove
  },
  [BLACK] = {
    [HIGHLIGHT_NORMAL] = palette.blackBG,
    [HIGHLIGHT_SELECTED] = palette.blackBGSelected,
    [HIGHLIGHT_PREV_MOVE] = palette.blackBGPrevMove
  }
}

local CELL_FOREGROUNDS = {
  [WHITE] = palette.whiteFG,
  [BLACK] = palette.blackFG
}

local function drawHalfBoardCells(idx, board)
  local screenBound = false

  local y0 = idx * 4 - 3
  local y1 = idx * 4

  for x = 1, 8 do
    for y = y0, y1 do
      local cellIdx = idx2dx64(x, y)

      local entry = board[cellIdx] or {}
      local highlight = entry.highlight or HIGHLIGHT_NORMAL
      local marker = entry.marker or false

      local cachedEntry = cachedBoardCells[cellIdx]

      if cachedEntry and cachedEntry.highlight == highlight and cachedEntry.marker == marker then
        goto continue
      end

      cachedBoardCells[cellIdx] = { highlight = highlight, marker = marker }

      local color = getCellColor(x, y)

      local bg = CELL_BACKGROUNDS[color][highlight]
      local fg = CELL_FOREGROUNDS[color]

      if not screenBound then
        gpuBind(screens[idx], false)
        screenBound = true
      end

      drawCell(x, y, bg, fg)

      if marker then
        drawMarker(x, y, CELL_BACKGROUNDS[color][HIGHLIGHT_SELECTED])
      end

      ::continue::
    end
  end
end

local function drawBoardCells(board)
  drawHalfBoardCells(1, board)
  drawHalfBoardCells(2, board)
end

-- drawing the pieces
--------------------------------------------------------------------------------

local function loadModel(path, color)
  local data = {}

  color = string.char(color)

  for line in io.lines(path) do
    local x, y, z = line:match("(-*%d+) (-*%d+) (-*%d+)")
    x = math.max(0, math.min(47, x + 12))
    y = math.max(0, math.min(47, y + 12))
    z = math.max(0, math.min(31, z + 4))
    local idx = x * 24 + y + 1

    if not data[idx] then
      data[idx] = {}
    end

    data[idx][z + 1] = color
  end

  for i, col in pairs(data) do
    for i = 1, 32 do
      if not col[i] then
        col[i] = "\0"
      end
    end
    data[i] = table.concat(col)
  end

  os.sleep(0)

  return data
end

local function loadModels()
  if DEBUG and GLOBAL_MODELS then
    return GLOBAL_MODELS
  end

  local models = {
    [PAWN] = {
      [WHITE] = loadModel("pieces/pawn.txt", WHITE),
      [BLACK] = loadModel("pieces/pawn.txt", BLACK)
    },
    [KNIGHT] = {
      [WHITE] = loadModel("pieces/knight.txt", WHITE),
      [BLACK] = loadModel("pieces/knight.txt", BLACK)
    },
    [BISHOP] = {
      [WHITE] = loadModel("pieces/bishop.txt", WHITE),
      [BLACK] = loadModel("pieces/bishop.txt", BLACK)
    },
    [ROOK] = {
      [WHITE] = loadModel("pieces/rook.txt", WHITE),
      [BLACK] = loadModel("pieces/rook.txt", BLACK),
    },
    [QUEEN] = {
      [WHITE] = loadModel("pieces/queen.txt", WHITE),
      [BLACK] = loadModel("pieces/queen.txt", BLACK)
    },
    [KING] = {
      [WHITE] = loadModel("pieces/king.txt", WHITE),
      [BLACK] = loadModel("pieces/king.txt", BLACK)
    }
  }

  if DEBUG then
    GLOBAL_MODELS = models
  end

  return models
end

local function appendModelData(dst, model, sx, sy)
  local empty = ("\0"):rep(32)
  for x = sx, sx + 23 do
    for y = sy, sy + 23 do
      dst[x * 48 + y + 1] = model[(x - sx) * 24 + y - sy + 1] or empty
    end
  end
end

local function compileVoxelData(models)
  local data = {}
  local empty = ("\0"):rep(32)
  appendModelData(data, models[1] or {}, 0, 0)
  appendModelData(data, models[2] or {}, 24, 0)
  appendModelData(data, models[3] or {}, 0, 24)
  appendModelData(data, models[4] or {}, 24, 24)
  return table.concat(data)
end

local PROJECTOR_SCALE = 2/3 * 119/128
local PROJECTOR_OFFSETS = {
  {1309/8192, 2499/8192},
  {119/512, 2499/8192},
  {2499/8192, 2499/8192},
  {3094/8192, 2499/8192},
  {1309/8192, 119/512},
  {119/512, 119/512},
  {2499/8192, 119/512},
  {3094/8192, 119/512},
}

local function setupProjectors()
  for i, projector in ipairs(projectors) do
    component.invoke(projector, "clear")
    component.invoke(projector, "setScale", PROJECTOR_SCALE)
    local x, z = table.unpack(PROJECTOR_OFFSETS[(i - 1) % 8 + 1])
    component.invoke(projector, "setTranslation", x, 1, z)
    component.invoke(projector, "setPaletteColor", BLACK, 0xf66b1d)
    component.invoke(projector, "setPaletteColor", WHITE, 0x68817c)
  end
end


local cachedBoardPieces = {}
local EMPTY_TABLE = {}

local function drawPieces(models, board)
  for py = 1, 4 do
    for px = 1, 4 do
      local pIdx = idx2dx16(px, py)
      local projector = projectors[pIdx]

      local data = {}
      local kind, color = getPiece(board, px * 2, py * 2 - 1)
      data[1] = kind and models[kind][color] or EMPTY_TABLE
      local kind, color = getPiece(board, px * 2 - 1, py * 2 - 1)
      data[2] = kind and models[kind][color] or EMPTY_TABLE
      local kind, color = getPiece(board, px * 2, py * 2)
      data[3] = kind and models[kind][color] or EMPTY_TABLE
      local kind, color = getPiece(board, px * 2 - 1, py * 2)
      data[4] = kind and models[kind][color] or EMPTY_TABLE

      if cachedBoardPieces[pIdx] then
        local cachedData = cachedBoardPieces[pIdx]
        for i = 1, 4 do
          if cachedData[i] ~= data[i] then
            break
          elseif i == 4 then
            goto continue
          end
        end
      end

      cachedBoardPieces[pIdx] = data

      local data = compileVoxelData(data)
      component.invoke(projector, "setRaw", data)

      ::continue::
    end
  end
end

-- game logic
--------------------------------------------------------------------------------

local gpuState = saveGpuState()
initBoardScreen(1)
initBoardScreen(2)
setupProjectors()
local models = loadModels()

local board = newBoard()
drawBoardCells({})
drawPieces(models, board)

local moveX, moveY
local validMoves = {}

while true do
  local _, screen, x, y = event.pull("touch")
  local x = (x - 1) // 10 + 1
  local y = 4 - (y - 1) // 5
  if screen == screens[2] then
    y = y + 4
  end

  if not moveX and getPieceKind(board, x, y) then
    moveX, moveY = x, y
    validMoves = getValidMoves(board, x, y)

    local cells = {}
    cells[idx2dx64(x, y)] = { highlight = HIGHLIGHT_SELECTED }

    for _, move in ipairs(validMoves) do
      local idx = idx2dx64(move[1], move[2])
      if not cells[idx] then cells[idx] = {} end
      cells[idx].marker = true
    end

    drawBoardCells(cells)
  elseif moveX then
    local moveType = classifyMove(board, moveX, moveY, x, y)

    if moveType ~= MOVE_ILLEGAL then
      local pieceType, color = getPiece(board, moveX, moveY)
      local rookY = color == WHITE and 1 or 8

      movePiece(board, moveX, moveY, x, y)

      if moveType == MOVE_CASTLING_QUEENSIDE then
        movePiece(board, 1, rookY, 4, rookY)
      elseif moveType == MOVE_CASTLING_KINGSIDE then
        movePiece(board, 8, rookY, 6, rookY)
      end

      if pieceType == KING then
        board.castlingRights[color][CASTLING_KINGSIDE] = false
        board.castlingRights[color][CASTLING_QUEENSIDE] = false
      end

      if pieceType == ROOK then
        if moveX == 1 then
          board.castlingRights[color][CASTLING_QUEENSIDE] = false
        elseif moveX == 8 then
          board.castlingRights[color][CASTLING_KINGSIDE] = false
        end
      end

      drawPieces(models, board)
    end

    moveX = nil
    validMoves = {}
    drawBoardCells({})
  end
end

restoreGpuState(gpuState)
require("term").clear()
